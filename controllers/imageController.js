require('dotenv').config();

// 1. Configuration AWS SDK
const AWS = require('aws-sdk');
const s3 = new AWS.S3({
    region: process.env.AWS_REGION,
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
});


// 2. Fonction pour télécharger une image vers S3
const uploadImageToS3 = async (file) => {
    const params = {
        Bucket: process.env.AWS_BUCKET_NAME,
        Key: `pokemons_images/${file.originalname}`,
        Body: file.buffer,
        ACL: 'public-read' // Rendre l'image accessible publiquement
    };

    const result = await s3.upload(params).promise();
    return result.Location; // Retourne l'URL de l'image téléchargée
};




// 3. Contrôleur pour gérer le téléchargement d'images
exports.uploadImage = async (req, res) => {
    try {
        const imageUrl = await uploadImageToS3(req.file);
        res.json({ imageUrl });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Erreur lors du téléchargement de l\'image.' });
    }
};